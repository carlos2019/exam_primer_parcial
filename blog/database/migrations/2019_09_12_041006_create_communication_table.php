<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommunicationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('communication', function (Blueprint $table) {
            $table->bigIncrements('id_communication');
            $table->string('name_communication',100)->nullable(true);
            $table->string('twilio_phone',100)->nullable(true);
            $table->boolean('call_whisper')->nullable(true);
            $table->string('call_whisper_delay',100)->nullable(true);
            $table->boolean('voicemail')->nullable(true);
            $table->string('voicemail_delay',100)->nullable(true);
            $table->string('voicemail_file',100)->nullable(true);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('communication');
    }
}
